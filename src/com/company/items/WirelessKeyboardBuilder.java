package com.company.items;

public class WirelessKeyboardBuilder implements WirelessItemBuilder {

    private WirelessKeyboard wirelessKeyboard; // instance of wireless keyboard

    // constructor
    public WirelessKeyboardBuilder() {
        this.wirelessKeyboard = new WirelessKeyboard();
    }

    /**
     * Method to define id for a wireless item
     * @param id of wireless item
     */
    @Override
    public void defineId(String id) {
        this.wirelessKeyboard.setId(id);
    }

    /**
     * Method to define name for a wireless item
     * @param name of wireless item
     */
    @Override
    public void defineName(String name) {
        this.wirelessKeyboard.setName(name);
    }

    /**
     * Method to define price for a wireless item
     * @param price of wireless item
     */
    @Override
    public void definePrice(double price) {
        this.wirelessKeyboard.setPrice(price);
    }

    /**
     * Method to define range for a wireless item
     * @param range of wireless item
     */
    @Override
    public void defineRange(double range) {
        this.wirelessKeyboard.setRange(range);
    }

    /**
     * Method to define batteryTimeInDays for a wireless item
     * @param batteryTimeInDays of wireless item
     */
    @Override
    public void defineBatteryTimeInDays(int batteryTimeInDays) {
        this.wirelessKeyboard.setBatteryTimeInDays(batteryTimeInDays);
    }

    /**
     * Method to build a wireless keyboard
     * @param id of a wireless keyboard
     * @param name of a wireless keyboard
     * @param price of a wireless keyboard
     * @param range of a wireless keyboard
     * @param batteryTimeInDays of a wireless keyboard
     */
    @Override
    public void build(String id, String name, double price, double range, int batteryTimeInDays) {
        this.defineId(id);
        this.defineName(name);
        this.definePrice(price);
        this.defineRange(range);
        this.defineBatteryTimeInDays(batteryTimeInDays);
    }

    /**
     * Method to get a wireless item
     * @return instance of a wireless keyboard
     */
    @Override
    public WirelessItem getWirelessItem() {
        return this.wirelessKeyboard;
    }
}
