package com.company.items;

public class WirelessMouse extends WirelessItem {

    // constructor
    public WirelessMouse() {}

    /**
     * Method to set an id of a wireless mouse
     * @param id of wireless mouse
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method to set a name of a wireless mouse
     * @param name of wireless mouse
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to set a price of a wireless mouse
     * @param price of wireless mouse
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Method to set a range of a wireless mouse
     * @param range of wireless mouse
     */
    public void setRange(double range) {
        this.range = range;
    }

    /**
     * Method to set batteryTimeInDays of a wireless mouse
     * @param batteryTimeInDays of wireless mouse
     */
    public void setBatteryTimeInDays(int batteryTimeInDays) {
        this.batteryTimeInDays = batteryTimeInDays;
    }
}