package com.company.items;

public class WiredMouse extends WiredItem {

    // constructor
    public WiredMouse(String itemId, String itemName, double itemPrice, double lengthOfWire) {
        super(itemId, itemName, itemPrice);
        this.lengthOfWire = lengthOfWire;
    }
}