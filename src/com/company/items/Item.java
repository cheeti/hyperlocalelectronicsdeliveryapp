package com.company.items;

public class Item {

    String id; // id of an item
    String name; // name of an item
    double price; // price of an item

    // constructor
    public Item() {
    }

    // constructor
    public Item(String itemId, String itemName, double itemPrice) {
        id = itemId;
        name = itemName;
        price = itemPrice;
    }

    /**
     * Method to get id of an item
     * @return id of the item
     */
    public String getId() {
        return id;
    }

    /**
     * Method to get name of an item
     * @return name of the item
     */
    public String getName() {
        return name;
    }

    /**
     * Method to get price of an item
     * @return price of the item
     */
    public double getPrice() {
        return price;
    }

    /**
     * Method to display an item's details
     */
    public void displayItemDetails() {
        System.out.println(id + "\t" + name + "\t\t\t" + price);
    }
}