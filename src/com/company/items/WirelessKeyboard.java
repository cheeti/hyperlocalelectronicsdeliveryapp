package com.company.items;

public class WirelessKeyboard extends WirelessItem {

    //constructor
    public WirelessKeyboard() {}

    /**
     * Method to set an id of a wireless keyboard
     * @param id of wireless keyboard
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method to set a name of a wireless keyboard
     * @param name of wireless keyboard
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to set a price of a wireless keyboard
     * @param price of wireless keyboard
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Method to set a range of a wireless keyboard
     * @param range of wireless keyboard
     */
    public void setRange(double range) {
        this.range = range;
    }

    /**
     * Method to set batteryTimeInDays of a wireless keyboard
     * @param batteryTimeInDays of wireless keyboard
     */
    public void setBatteryTimeInDays(int batteryTimeInDays) {
        this.batteryTimeInDays = batteryTimeInDays;
    }
}