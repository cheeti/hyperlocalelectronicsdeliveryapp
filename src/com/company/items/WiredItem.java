package com.company.items;

// class for a wired item
public abstract class WiredItem extends Item {

    double lengthOfWire; // length of a wire

    // constructor
    public WiredItem(String itemId, String itemName, double itemPrice) {
        super(itemId, itemName, itemPrice);
    }
}