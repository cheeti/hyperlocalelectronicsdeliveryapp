package com.company.items;

public interface WirelessItemBuilder {

    void defineId(String id);
    void defineName(String name);
    void definePrice(double price);
    void defineRange(double range);
    void defineBatteryTimeInDays(int batteryTimeInDays);

    void build(String id, String name, double price, double range, int batteryTimeInDays);

    WirelessItem getWirelessItem();
}
