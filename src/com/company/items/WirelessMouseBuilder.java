package com.company.items;

public class WirelessMouseBuilder implements WirelessItemBuilder {

    private WirelessMouse wirelessMouse; // instance of wireless mouse

    // constructor
    public WirelessMouseBuilder() {
        this.wirelessMouse = new WirelessMouse();
    }

    /**
     * Method to define id for a wireless item
     * @param id of wireless item
     */
    @Override
    public void defineId(String id) {
        this.wirelessMouse.setId(id);
    }

    /**
     * Method to define name for a wireless item
     * @param name of wireless item
     */
    @Override
    public void defineName(String name) {
        this.wirelessMouse.setName(name);
    }

    /**
     * Method to define price for a wireless item
     * @param price of wireless item
     */
    @Override
    public void definePrice(double price) {
        this.wirelessMouse.setPrice(price);
    }

    /**
     * Method to define range for a wireless item
     * @param range of wireless item
     */
    @Override
    public void defineRange(double range) {
        this.wirelessMouse.setRange(range);
    }

    /**
     * Method to define batteryTimeInDays for a wireless item
     * @param batteryTimeInDays of wireless item
     */
    @Override
    public void defineBatteryTimeInDays(int batteryTimeInDays) {
        this.wirelessMouse.setBatteryTimeInDays(batteryTimeInDays);
    }

    /**
     * Method to build a wireless mouse
     * @param id of a wireless mouse
     * @param name of a wireless mouse
     * @param price of a wireless mouse
     * @param range of a wireless mouse
     * @param batteryTimeInDays of a wireless mouse
     */
    @Override
    public void build(String id, String name, double price, double range, int batteryTimeInDays) {
        this.defineId(id);
        this.defineName(name);
        this.definePrice(price);
        this.defineRange(range);
        this.defineBatteryTimeInDays(batteryTimeInDays);
    }

    /**
     * Method to get a wireless item
     * @return instance of a wireless mouse
     */
    @Override
    public WirelessItem getWirelessItem() {
        return this.wirelessMouse;
    }
}
