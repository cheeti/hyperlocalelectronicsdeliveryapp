package com.company.items;

public class WiredKeyboard extends WiredItem {

    // constructor
    public WiredKeyboard(String itemId, String itemName, double itemPrice, double lengthOfWire) {
        super(itemId, itemName, itemPrice);
        this.lengthOfWire = lengthOfWire;
    }
}