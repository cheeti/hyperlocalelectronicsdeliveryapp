package com.company;

//class to convert currency
public class CurrencyConverter implements Currency {

    //Exchange rate expression that interprets currency symbol into exchange rate
    private ExchangeRate rate;

    // constructor
    public CurrencyConverter(ExchangeRate rate) {
        this.rate = rate;
    }

    /**
     * Method to get converted currency value using exchange rate and rupees amount
     * @param inrAmount: amount in INR
     * @return converted currency
     */
    public double getValue(double inrAmount) {
        return inrAmount * (1/this.rate.interpret());
    }
}
