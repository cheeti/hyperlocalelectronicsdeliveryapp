package com.company;

// class for removing an order for an item
public class RemoveItemOrder implements Order {

    CustomerOrder customerOrder; // instance of customer order
    OrderItem itemToRemove; // item to be removed

    // constructor
    public RemoveItemOrder(CustomerOrder customerOrder, OrderItem orderItem) {
        this.customerOrder = customerOrder;
        itemToRemove = orderItem;
    }

    /**
     * Method to perform cancel action
     */
    public void performAction() {
        this.customerOrder.removeItemFromOrder(itemToRemove);
    }
}