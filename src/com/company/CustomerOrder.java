package com.company;

import java.util.ArrayList;

// class for Customer Order
public class CustomerOrder implements Container {

    // List of all order items
    ArrayList<OrderItem> itemsInOrder = new ArrayList<>();

    // Observer that observes changes in order
    private OrderObserver observer;

    // constructor
    CustomerOrder(OrderObserver observer) {
        this.observer = observer;
    }

    @Override
    public CustomIterator getIterator() {
        return new OrderItemIterator();
    }

    /**
     * Method to return total number of objects in the order
     * @return count of total items in order
     */
    int getTotalItemsInOrder() {
        return this.itemsInOrder.size();
    }

    /**
     * Method to add item in order item list.
     * @param item: new item to be added
     */
    void addItemToOrder(OrderItem item) {

        boolean itemAlreadyInOrderList = false;
        CustomIterator it = this.getIterator();

        while (it.hasNext()) {
            OrderItem currentOrderItem = (OrderItem) it.next();

            // if the item ID exists in the order list, update its quantity
            if (currentOrderItem.item.getId() == item.item.getId()) {
                itemAlreadyInOrderList = true;
                currentOrderItem.orderQuantity += item.orderQuantity;

                currentOrderItem.updateItem();
                break;
            }
        }

        // if the item ID doesn't exist in the order list, add a new item to the list
        if (!itemAlreadyInOrderList) {
            this.itemsInOrder.add(item);

            item.addItem();
        }
        observer.calculateOrderValue();
    }

    /**
     * Method to remove item from the list of order items
     * @param item: item to be removed
     */
    void removeItemFromOrder(OrderItem item) {
        this.itemsInOrder.remove(item);
        item.removeItem();
        observer.calculateOrderValue();
    }

    /**
     * Method to calculate total amount before taxes of all the order items
     * @return total amount before taxes
     */
    double calculateAmount() {
        double totalAmount = 0.0;
        for (OrderItem item : this.itemsInOrder) {
            totalAmount += item.getOrderAmount();
        }
        return totalAmount;
    }

    /**
     * Method to calculate tax on the total amount of the order
     * @param amount: total amount before taxes
     * @return total tax on the order
     */
    double calculateTax(double amount) {
        return amount * 0.12;
    }

    /**
     * Method to calculate total amount payable by the customer
     * @param totalAmount: total amount before taxes
     * @param tax: total tax on the amount
     * @return total amount payable by the customer
     */
    double calculateAmountPayable(double totalAmount, double tax) {
        return totalAmount + tax;
    }

    // class for iterating through the list of items
    private class OrderItemIterator implements CustomIterator {

        int index;

        /**
         * Method to check whether the list of items contains a succeeding item to the current one
         * @return TRUE if the next item exists; FALSE otherwise
         */
        @Override
        public boolean hasNext() {
            return index < itemsInOrder.size();
        }

        /**
         * Method to get the next item from the list of items
         * @return the item object if it exists; NULL otherwise
         */
        @Override
        public Object next() {
            if (this.hasNext()) {
                return itemsInOrder.get(index++);
            }
            return null;
        }
    }
}