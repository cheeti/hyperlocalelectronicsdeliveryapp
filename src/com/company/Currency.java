package com.company;

//interface to convert currency
interface Currency {
    public double getValue(double inrAmount);
}