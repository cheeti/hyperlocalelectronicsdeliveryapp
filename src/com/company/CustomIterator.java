package com.company;

// interface for an Iterator having abstract methods for traversal
interface CustomIterator {
    boolean hasNext();
    Object next();
}
