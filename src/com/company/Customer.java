package com.company;
import java.util.ArrayList;

// class for a customer
public class Customer implements OrderObserver {

    //order object for the customer
    CustomerOrder customerOrder;

    //total amount of the order before taxes
    private double totalAmount = 0.0;

    //total tax for the order
    private double totalTax = 0.0;

    //total amount payable by the customer
    private double totalAmountPayable = 0.0;

    // ArrayList containing the list of all orders to be processed
    private ArrayList<Order> orderList = new ArrayList<>();

    //constructor
    Customer() {
        this.customerOrder = new CustomerOrder(this);
    }

    /**
     * Method to take an order and add it to the order list
     * @param order: order to be added to rhe list of orders
     */
    void makeOrder(Order order) {
        orderList.add(order);
        order.performAction();
    }

    /**
     * Method to get the total amount for an order by a customer
     * @return total order amount for a customer
     */
    public double getTotalAmount() {
        return this.totalAmount;
    }

    /**
     * Method to get the total tax for an order by a customer
     * @return total tax for a customer
     */
    public double getTotalTax() {
        return this.totalTax;
    }

    /**
     * Method to get total payable amount by a customer
     * @return total amount payable
     */
    public double getTotalAmountPayable() {
        return this.totalAmountPayable;
    }

    /**
     * Method that is called after a change in customer order
     */
    @Override
    public void calculateOrderValue() {
        this.totalAmount = this.customerOrder.calculateAmount();
        this.totalTax = this.customerOrder.calculateTax(this.totalAmount);
        this.totalAmountPayable = this.customerOrder.calculateAmountPayable(this.totalAmount, this.totalTax);
    }
}