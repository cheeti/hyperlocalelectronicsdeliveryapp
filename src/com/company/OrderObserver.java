package com.company;

//observer interface
interface OrderObserver {
    void calculateOrderValue();
}
