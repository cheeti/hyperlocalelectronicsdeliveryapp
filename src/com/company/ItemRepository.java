package com.company;

import com.company.items.Item;

import java.util.ArrayList;

// class acting as repository for items
class ItemRepository implements Container {

    // ArrayList containing list of information about all the items
    ArrayList<Item> itemsList;

    // constructor
    public ItemRepository(ArrayList<Item> items) {
        itemsList = items;
    }

    /**
     * Method to get the iterator for an item
     * @return item iterator
     */
    @Override
    public CustomIterator getIterator() {
        return new ItemIterator();
    }


    // class for iterating through the list of items
    private class ItemIterator implements CustomIterator {

        int index;

        /**
         * Method to check whether the list of items contains a succeeding item to the current one
         * @return TRUE if the next item exists; FALSE otherwise
         */
        @Override
        public boolean hasNext() {
            return index < itemsList.size();
        }

        /**
         * Method to get the next item from the list of items
         * @return the item object if it exists; NULL otherwise
         */
        @Override
        public Object next() {
            if(this.hasNext()) {
                return itemsList.get(index++);
            }
            return null;
        }
    }
}