package com.company;

// class for adding an order for an item
public class AddItemOrder implements Order {

    CustomerOrder customerOrder; // instance of customer order
    OrderItem itemToAdd; // item to be added

    // constructor
    public AddItemOrder(CustomerOrder customerOrder, OrderItem orderItem) {
        this.customerOrder = customerOrder;
        itemToAdd = orderItem;
    }

    /**
     * Method to perform an order action
     */
    public void performAction() {
        this.customerOrder.addItemToOrder(itemToAdd);
    }
}