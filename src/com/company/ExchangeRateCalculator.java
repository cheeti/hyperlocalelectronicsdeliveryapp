package com.company;

//interface for exchange rate calculator to interpret currency symbol
interface ExchangeRateCalculator {
    public double interpret();
}