package com.company;

import com.company.items.Item;

import java.util.ArrayList;

// class for a store
public class Store {

    String id; // id of a store
    String name; // name of a store
    String location; // location of a store
    ItemRepository itemRepo; // repository of different items in a store

    // constructor
    public Store(String storeId, String storeName, String storeLocation, ArrayList<Item> items) {
        id = storeId;
        name = storeName;
        location = storeLocation;
        itemRepo = new ItemRepository(items);
    }

    /**
     * Method to get the id of a store
     * @return id of the store
     */
    public String getId() {
        return id;
    }

    /**
     * Method to get the name of a store
     * @return name of the store
     */
    public String getName() {
        return name;
    }

    /**
     * Method to get the location of a store
     * @return location of the store
     */
    public String getLocation() {
        return location;
    }

    /**
     * Method to get the repository containing all items in a store
     * @return repository of items
     */
    public ItemRepository getItemRepository() {
        return itemRepo;
    }

    /**
     * Method to display a store's details
     */
    public void displayDetails() {
        System.out.println("\nID: " + id + "\nName: " + name + "\nLocation: " + location);
    }
};