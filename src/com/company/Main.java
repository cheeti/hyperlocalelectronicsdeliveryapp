package com.company;

import com.company.items.*;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.*;

public class Main {

    static StoreRepository storesDB; // database containing all stores' information
    static Store selectedStore; // store selected by the user
    static boolean isOrderPlaced = false; // flag to check if the order is placed or not
    static boolean isOrderPaid = false; // flag to check if the order amount is paid or not
    static Customer customer = new Customer(); // customer of an order

    public static void main(String[] args) {
        initializeDB();
        processMenu();
    }

    /**
     * Method to show the menu options to the user
     */
    static void showMenuOptions() {
        System.out.println("\nChoose an option:");
        System.out.println("1. Search all stores");
        System.out.println("2. Select a store");
        System.out.println("3. View all items in a store");
        System.out.println("4. Add an item from a store");
        System.out.println("5. Place an order");
        System.out.println("6. Pay for an order");
        System.out.println("7. Cancel an order");
        System.out.println("Press 0 to EXIT!");
    }

    /**
     * Method to process menu
     */
    static void processMenu() {
        while (true) {

            showMenuOptions();

            Scanner sc = new Scanner(System.in);
            char choice = sc.next().charAt(0);

            switch (choice) {

                // search all stores
                case '1': {
                    System.out.println("List of all stores:");
                    for (CustomIterator iter = storesDB.getIterator(); iter.hasNext(); ) {
                        Store currentStore = (Store) iter.next();
                        currentStore.displayDetails();
                    }

                    break;
                }

                // select a store
                case '2': {
                    System.out.println("Give ID of the store to be selected: ");
                    String storeId = sc.next();

                    boolean storeFound = false;
                    for (CustomIterator iter = storesDB.getIterator(); iter.hasNext(); ) {
                        Store currentStore = (Store) iter.next();

                        if (currentStore.getId().equals(storeId)) { // if the store ID entered by the user exists
                            storeFound = true;
                            selectedStore = currentStore;
                            System.out.println("Details of the store selected by you:");
                            currentStore.displayDetails();
                            break;
                        }
                    }

                    if (!storeFound)
                        System.out.println("Entered StoreID doesn't exist in database!");

                    break;
                }

                // view all items in a store
                case '3': {
                    if (selectedStore != null) {
                        System.out.println("List of all the items in the selected store (" + selectedStore.getId() + "):");
                        System.out.println("\nItemID\tName\t\t\t\t\t\t\tPrice");

                        for (CustomIterator itemIter = selectedStore.getItemRepository().getIterator(); itemIter.hasNext(); ) {
                            Item currentItem = (Item) itemIter.next();
                            currentItem.displayItemDetails();
                        }
                    } else {
                        System.out.println("Please select a store first!");
                    }

                    break;
                }

                // add an item from a store
                case '4': {
                    if (selectedStore != null) {
                        boolean addMoreItems;

                        addItemFromStore();

                        do {
                            System.out.println("\nAdd another item? Press y for Yes and n for No.");
                            char addAnotherItemChoice = sc.next().charAt(0);

                            switch (addAnotherItemChoice) {

                                case 'Y':
                                case 'y': {
                                    addItemFromStore();
                                    addMoreItems = true;
                                    break;
                                }

                                case 'N':
                                case 'n': {
                                    addMoreItems = false;
                                    break;
                                }

                                default: {
                                    addMoreItems = true;
                                    System.out.println("Incorrect choice entered! Enter either y or n!");
                                }
                            }

                        } while (addMoreItems);
                    } else {
                        System.out.println("Please select a store first!");
                    }

                    break;
                }

                // place an order
                case '5': {
                    if (selectedStore != null) {
                        if (customer.customerOrder.getTotalItemsInOrder() != 0) {
                            System.out.println("\nItems in your list: ");
                            System.out.println("\nItemID\tName\tQuantity\tPrice\tAmount");

                            // display all ordered items' details
                            CustomIterator it = customer.customerOrder.getIterator();
                            while (it.hasNext()) {
                                OrderItem orderItem = (OrderItem) it.next();
                                orderItem.displayOrderItemDetails();
                            }

                            System.out.println("\nAre you sure to place your order?\nPress y for Yes and n for No.");
                            char placeOrderChoice = sc.next().charAt(0);

                            switch (placeOrderChoice) {

                                case 'Y':
                                case 'y': {
                                    isOrderPlaced = true; // order placed
                                    System.out.println("\nTotal order value = Rs. " + customer.getTotalAmount() +
                                            "\nTotal tax = Rs. " + customer.getTotalTax() + "\nTotal amount payable = Rs. " +
                                            customer.getTotalAmountPayable());
                                    break;
                                }

                                case 'N':
                                case 'n': {
                                    break;
                                }

                                default: {
                                    System.out.println("Incorrect choice entered!");
                                }
                            }
                        } else {
                            System.out.println("Please add atleast one item from a store!");
                        }
                    } else {
                        System.out.println("Please select a store first!");
                    }

                    break;
                }

                // pay for the order
                case '6': {
                    if (selectedStore != null) {
                        if (customer.customerOrder.getTotalItemsInOrder() != 0) {
                            if (!isOrderPaid) {
                                System.out.println("\nWhich currency you want to pay in?\nPress d for Dollar, r for Rupees, e for Euros");
                                char currencySelected = sc.next().charAt(0);
                                if (currencySelected != 'd' && currencySelected != 'D' &&
                                        currencySelected != 'r' && currencySelected != 'R' &&
                                        currencySelected != 'e' && currencySelected != 'E') {
                                    System.out.println("Wrong currency selected! Please enter valid option!");
                                } else {
                                    ExchangeRate exchangeRate = new ExchangeRate(currencySelected);
                                    CurrencyConverter currencyConverter = new CurrencyConverter(exchangeRate);
                                    System.out.println("\nYou need to pay: " + currencyConverter.getValue(customer.getTotalAmountPayable()) + " in the currency you selected!");
                                    System.out.println("\nDo you want to proceed?\nPress y for Yes and n for No: ");
                                    char payOrderChoice = sc.next().charAt(0);

                                    switch (payOrderChoice) {

                                        case 'Y':
                                        case 'y': {
                                            isOrderPaid = true; // order paid
                                            System.out.println("\nOrder paid. Congratulations!!");
                                            break;
                                        }

                                        case 'N':
                                        case 'n': {
                                            break;
                                        }

                                        default: {
                                            System.out.println("Incorrect choice entered!");
                                        }
                                    }
                                }
                            } else {
                                System.out.println("\nYour order is already paid.");
                            }
                        } else {
                            System.out.println("Please add atleast one item from a store!");
                        }
                    } else {
                        System.out.println("Please select a store first!");
                    }
                    break;
                }

                // cancel the order
                case '7': {
                    if (selectedStore != null) {
                        if (customer.customerOrder.getTotalItemsInOrder() != 0) {
                            if (isOrderPlaced) {
                                System.out.println("\nItems in your list: ");
                                System.out.println("\nItemID\tName\tQuantity\tPrice\tAmount");

                                // display all ordered items' details
                                CustomIterator it = customer.customerOrder.getIterator();
                                while (it.hasNext()) {
                                    OrderItem orderItem = (OrderItem) it.next();
                                    orderItem.displayOrderItemDetails();
                                }

                                // take IDs of all the items to be cancelled
                                System.out.println("Give IDs of all the items to be cancelled separated by comma: ");
                                String cancelItemIdsArr = sc.next();

                                List<String> cancelItemIdsArrList = Arrays.asList(cancelItemIdsArr.split("\\s*,\\s*"));

                                System.out.println("Are you sure to cancel your order?\nPress y for Yes and n for No.");
                                char cancelOrderChoice = sc.next().charAt(0);

                                switch (cancelOrderChoice) {

                                    case 'Y':
                                    case 'y': {
                                        // for each Item ID given by the user, iterate over the orders list
                                        for (String cancelItemId : cancelItemIdsArrList) {
                                            Iterator cancelOrderIterator = customer.customerOrder.itemsInOrder.iterator();
                                            RemoveItemOrder removeItemOrder = null;

                                            while (cancelOrderIterator.hasNext()) {
                                                OrderItem orderItem = (OrderItem) cancelOrderIterator.next();

                                                // if the ordered item's ID matches with the ID of the item to be cancelled,
                                                // process a new cancel order
                                                if (orderItem.item.getId().equals(cancelItemId)) {
                                                    removeItemOrder = new RemoveItemOrder(customer.customerOrder, orderItem);
                                                    break;
                                                }
                                            }
                                            if (removeItemOrder != null) {
                                                customer.makeOrder(removeItemOrder);
                                            }
                                        }

                                        break;
                                    }

                                    case 'N':
                                    case 'n': {
                                        break;
                                    }

                                    default: {
                                        System.out.println("Incorrect choice entered!");
                                    }
                                }
                            } else {
                                System.out.println("No order to cancel! Please place an order first!");
                            }
                        } else {
                            System.out.println("Please add atleast one item from a store!");
                        }
                    } else {
                        System.out.println("Please select a store first!");
                    }
                    break;
                }

                // exit application
                case '0': {
                    System.out.println("Thank you! See you again!");
                    System.exit(0);
                }

                // wrong choice entered
                default: {
                    System.out.println("Incorrect choice entered! Please enter correct choice!");
                }

                showMenuOptions();
            }
        }
    }

    /**
     * Method to add information about each store in the list of stores each containing a list of items
     */
    static void initializeDB() {

        // initializing list of items
        Item item1 = new WiredMouse("I101", "HP Edge Wireless Mouse", 350.0, 3);

        WirelessItemBuilder wirelessItemBuilder = new WirelessKeyboardBuilder();
        wirelessItemBuilder.build("I102", "Logitech Ultrafast Keyboard", 1000, 15, 100);
        Item item2 = wirelessItemBuilder.getWirelessItem();

        WirelessItemBuilder wirelessItemBuilder2 = new WirelessMouseBuilder();
        wirelessItemBuilder2.build("I103", "Razer Mamba Gaming Mouse", 1876.0, 15, 60);
        Item item3 = wirelessItemBuilder2.getWirelessItem();

        // storing all the items in an array list
        ArrayList<Item> itemsList1 = new ArrayList<Item>() {
            { add(item1); }
            { add(item2); }
            { add(item3); }
        };

        // initializing a store
        Store store1 = new Store("S101", "Vishal Mega Mart", "Bommanahalli", itemsList1);

        WirelessItemBuilder wirelessItemBuilder3 = new WirelessKeyboardBuilder();
        wirelessItemBuilder3.build("I104", "Apple Small Keyboard", 5000, 15, 100);
        Item item4 = wirelessItemBuilder3.getWirelessItem();

        WirelessItemBuilder wirelessItemBuilder4 = new WirelessMouseBuilder();
        wirelessItemBuilder4.build("I105", "Apple Magic Mouse", 3000, 15, 100);
        Item item5 = wirelessItemBuilder4.getWirelessItem();

        Item item6 = new WiredKeyboard("I106", "Lenovo Keyboard", 2500, 3);

        ArrayList<Item> itemsList2 = new ArrayList<Item>() {
            { add(item4); }
            { add(item5); }
            { add(item6); }
        };

        Store store2 = new Store("S102", "Reliance Fresh", "Electronic City", itemsList2);

        WirelessItemBuilder wirelessItemBuilder5 = new WirelessKeyboardBuilder();
        wirelessItemBuilder5.build("I107", "Dell Wireless Keyboard", 5000, 15, 50);
        Item item7 = wirelessItemBuilder5.getWirelessItem();

        Item item8 = new WiredMouse("I108", "Lenovo Wireless Mouse", 600, 2);
        Item item9 = new WiredKeyboard("I109", "Logitech Wired Keyboard", 2500, 3);

        ArrayList<Item> itemsList3 = new ArrayList<Item>() {
            { add(item7); }
            { add(item8); }
            { add(item9); }
        };

        Store store3 = new Store("S103", "Spencer's", "Koramangala", itemsList1);

        // add all stores in an array list
        ArrayList<Store> storesList = new ArrayList<>();
        storesList.add(store1);
        storesList.add(store2);
        storesList.add(store3);

        storesDB = new StoreRepository(storesList); // initialize storesDB with the list of all the stores
    }

    /**
     * Method to add an item from store
     */
    static void addItemFromStore() {
        System.out.println("\nGive ID of the item to add: ");
        Scanner sc = new Scanner(System.in);
        String itemId = sc.next();

        boolean itemFound = false;
        for (CustomIterator itemIter = selectedStore.getItemRepository().getIterator(); itemIter.hasNext(); ) {
            Item currentItem = (Item) itemIter.next();

            // if the given item ID exists in the selected store
            if (currentItem.getId().equals(itemId)) {

                itemFound = true;
                System.out.println("Give quantity of the item to be added: ");
                int orderQty = sc.nextInt();

                OrderItem orderItem = new OrderItem(currentItem, orderQty);
                AddItemOrder addItemOrder = new AddItemOrder(customer.customerOrder, orderItem);
                customer.makeOrder(addItemOrder);

                break;
            }
        }

        if (!itemFound) {
            System.out.println("Entered ItemID doesn't exist in the given store!");
        }
    }
}