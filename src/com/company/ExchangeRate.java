package com.company;

//class for exchange rate for its interpretation
public class ExchangeRate implements ExchangeRateCalculator {

    //Context for the interpreter
    private char currencySymbol;

    // constructor
    public ExchangeRate(char currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Method to interpret currency symbol into exchange rate of the currency
     * @return exchange rate of currency
     */
    @Override
    public double interpret() {
        switch (this.currencySymbol) {
            // Dollar
            case 'd':
            case 'D': {
                return 64.0; // 1 USD = 64.0 INR
            }

            // Rupee
            case 'r':
            case 'R': {
                return 1.0; // 1 INR = 1 INR
            }

            // Euro
            case 'e':
            case 'E': {
                return 80.0; // 1 EUR = 80.0 INR
            }

            // Rupee is default
            default: {
                return 1.0; // exchange rate for INR
            }
        }
    }
}