package com.company;

// interface for a Container having abstract method to return Iterator
interface Container {
    CustomIterator getIterator();
}