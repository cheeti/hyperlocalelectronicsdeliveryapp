package com.company;

import java.util.ArrayList;

// class acting as repository for stores
public class StoreRepository implements Container {

    // ArrayList containing list of information about all the stores
    ArrayList<Store> storesList;

    // constructor
    public StoreRepository(ArrayList<Store> stores) {
        storesList = stores;
    }

    /**
     * Method to get the iterator for a store
     * @return store iterator
     */
    @Override
    public CustomIterator getIterator() {
        return new StoreIterator();
    }


    // class for iterating through the list of stores
    private class StoreIterator implements CustomIterator {

        int index;

        /**
         * Method to check whether the list of stores contains a succeeding store to the current one
         * @return TRUE if the next store exists; FALSE otherwise
         */
        @Override
        public boolean hasNext() {
            return index < storesList.size();
        }

        /**
         * Method to get the next store from the list of stores
         * @return the next store object if it exists; NULL otherwise
         */
        @Override
        public Object next() {
            if(this.hasNext()) {
                return  storesList.get(index++);
            }
            return null;
        }
    }
}