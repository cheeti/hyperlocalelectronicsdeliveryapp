package com.company;

import com.company.items.Item;

// class for an item ordered
public class OrderItem {

    Item item; // base item
    int orderQuantity; // order quantity of an ordered item
    double orderAmount; // order amount of an ordered item

    // constructor
    public OrderItem(Item baseItem, int orderQty) {
        item = baseItem;
        orderQuantity = orderQty;
        orderAmount = orderQuantity * item.getPrice();
    }

    /**
     * Method to get the quantity for an ordered item
     * @return order quantity
     */
    public int getOrderQuantity() {
        return orderQuantity;
    }

    /**
     * Method to get the total amount for ordered item
     * @return order amount
     */
    public double getOrderAmount() {
        return orderQuantity * item.getPrice();
    }

    /**
     * Method to display ordered item's details
     */
    public void displayOrderItemDetails() {
        System.out.println(item.getId() + "\t" + item.getName() + "\t" + orderQuantity + "\t" + item.getPrice() + "\t" + getOrderAmount());
    }

    /**
     * Method to update an item
     */
    public void updateItem() {
        System.out.println("\nItem updated!\nItem ID: " + this.item.getId() + "\nItem Name: " + this.item.getName() + "\nQuantity: " + this.orderQuantity + "\nPrice per unit: Rs. " + this.item.getPrice() + "\nTotal Amount: Rs. " + this.getOrderAmount());
    }

    /**
     * Method to add an item
     */
    public void addItem() {
        System.out.println("\nItem added!\nItem ID: " + this.item.getId() + "\nItem Name: " + this.item.getName() + "\nQuantity: " + this.orderQuantity + "\nPrice per unit: Rs. " + this.item.getPrice() + "\nTotal Amount: Rs. " + this.getOrderAmount());
    }

    /**
     * Method to remove an item
     */
    public void removeItem() {
        System.out.println("\nItem removed!\nItem ID: " + this.item.getId() + "\nItem Name: " + this.item.getName() + "\nQuantity: " + this.orderQuantity + "\nPrice per unit: Rs. " + this.item.getPrice() + "\nTotal Refundable Amount: Rs. " + this.getOrderAmount());
    }
}